# -*- coding: utf-8 -*-
"""
Created on Fri Oct 25 14:04:42 2019

@author: DELL
"""

def test_one_plus_one_is_two():
    "Check that one and one are indeed two."
    assert 1 + 1 == 2
