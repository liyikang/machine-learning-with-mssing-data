# Machine Learning with mssing data

The main domain of this research is understanding the basic of machine learning with missing data. For example, people may choose perfect data when people want to indetify a huge amount of data by machine learning model. However, data may not perfect all the time. The main point of this research is how to solve that problem when some data is missing such as the data of human's face because sometimes only part of face were collected. 

For solving this problem, we used three methodogies. The first one is data simulation which is in the data simulation folder. We creates a different numerical sizes dataset which has five x values and y values. Then we randomly deleted different percentage of data from 20% to 95% to simulate the missing data. The second methodology is imputation methods. We used three methods to impute the missing data and stored the imputed data in imputed data folder. Finally, we used two machine learning models to make predictions based on the imputed data and compare the predictions with the original dataset by calculating the MSE. Then we made several plots to compare how different factors affect the MSE(mean square error) and the plots for different machine learning models are stored in different folders. 

The final report is the report for this project. The final presentation is a detailed presentation which contains everything we did for this research.

Note: Running the machine learning models will take many hours.