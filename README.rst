==================================
Machine learning with missing data
==================================

.. image:: https://img.shields.io/travis//machine-learning-with-missing-data.svg
        :target: https://travis-ci.org//machine-learning-with-missing-data

.. image:: https://img.shields.io/pypi/v/machine-learning-with-missing-data.svg
        :target: https://pypi.python.org/pypi/machine-learning-with-missing-data


Python package for doing science.

* Free software: 3-clause BSD license
* Documentation: (COMING SOON!) https://.github.io/machine-learning-with-missing-data.

Features
--------

* TODO
